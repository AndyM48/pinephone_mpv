# Pinephone MPV (pp_mpv)

A simple wrapper for MPV for the Pinephone and other Linux mobile devices

After installation mpv remains unaffected since pp_mpv uses its own, separate, configuration folder.

## Requirements

The only dependencies are tcl, tk and mpv.

## On Screen Controls (osc)

pp_mpv includes a modified version of the modernx osc by cyl0 (https://github.com/cyl0/ModernX/)

## Optional Additions

Autoload (mpv). Automatically loads playlist entries, and or directory files, before and after the currently played file.

Coverart (https://github.com/CogentRedTester/mpv-coverart). Automatically load external cover art files into mpv media player as additional video tracks. Will also load a set image if no coverart exists.

Thumbfast (https://github.com/po5/thumbfast). High-performance on-the-fly thumbnailer for mpv.

Recent versions of these are included in the pp_mpv download

## Screenshots

![Folders](screenshots/folders.jpg) ![Folder Listing](screenshots/files_folders.jpg) ![MPV](screenshots/mpv.jpg)
## Installaton

Download the source code and run install.sh to:

	Copy the pinephone_mpv programme to /usr/bin

	Copy the icons to /usr/share/pixmaps/pinephone_mpv 

	Copy the desktop file to /usr/share/applications

	Copy the configuration file to ~/.config/pp_mpv
	
	Copy the lua script files to ~/.config/pp_mpv/scripts
	
	Copy the script configuration files to ~/.config/pp_mpv/script_opts


To update: download the source code and run install.sh again. all data will be preserved.

To uninstall Pinephone_mpv run uninstall.sh from the downloads directory.

## Configuration

Configuration options for mpv and pp_mpv are saved in text files in ~/.config/pp_mpv: mpv.conf and pp_mpv.conf

>	mpv.conf is pre-configured with sane, recommended, options.

>	Edit pp_mpv and change these options to your favourite folders:
>>		audio_dir=
>>		image_dir=
>>		video_dir=

>	For example:
>>		audio_dir=/home/username/Music
>>		image_dir=/home/username/Pictures
>>		video_dir=/home/username/Videos

Configuration options for modernx are saved in ~/.config/pp_mpv/script_opts/osc.conf. Configurable options are generally pre-configured with sane options,but can be changed and experimented with. **Phosh users should change the option of "iamphosh" to yes**; iamphosh=yes to reserve the space necessary at the bottom of the screen for the phosh control.

Configuration options for autoload, coverart and thumbfast are saved in ~/.config/pp_mpv/script_opts and are preset to sane options, but feel free to experiment with them.


This is the first release of the software. If you find it useful, good. If you do not then please file a bug report.
