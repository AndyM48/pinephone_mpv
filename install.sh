#! /bin/sh

sudo cp pinephone_mpv /usr/bin
sudo chmod 0755 /usr/bin/pinephone_mpv
sudo mkdir -p /usr/share/pixmaps/pinephone_mpv
sudo cp icons/* /usr/share/pixmaps/pinephone_mpv
sudo cp pinephone_mpv.desktop /usr/share/applications
mkdir -p ${HOME}/.config/pp_mpv
# test if mpv.conf already exists
if [ -f  ${HOME}/.config/pp_mpv/mpv.conf ]; then
	# file exists
	if ! cmp -s "mpv.conf" "${HOME}/.config/pp_mpv/mpv.conf"; then
		cp mpv.conf  ${HOME}/.config/pp_mpv/mpv.conf.update
		echo "WARNING: mpv.conf saved as mpv.conf.update"
	fi
else
	cp mpv.conf  ${HOME}/.config/pp_mpv
fi
# test if pp_mpv.conf already exists
if [ -f  ${HOME}/.config/pp_mpv/pp_mpv.conf ]; then
	# file exists
	if ! cmp -s "pp_mpv.conf" "${HOME}/.config/pp_mpv/pp_mpv.conf"; then
		cp pp_mpv.conf  ${HOME}/.config/pp_mpv/pp_mpv.conf.update
		echo "WARNING: pp_mpv.conf saved as pp_mpv.conf.update"
	fi
else
	cp pp_mpv.conf  ${HOME}/.config/pp_mpv
fi
cp -r fonts ${HOME}/.config/pp_mpv
mkdir -p ${HOME}/.config/pp_mpv/scripts
cp scripts/modernx.lua ${HOME}/.config/pp_mpv/scripts
mkdir -p ${HOME}/.config/pp_mpv/script-opts
# test if osc.conf already exists
if [ -f  ${HOME}/.config/pp_mpv/script-opts/osc.conf ]; then
	# file exists
	if ! cmp -s "scripts-opts/osc.conf" "${HOME}/.config/pp_mpv/script-opts/osc.conf"; then
		cp script-opts/osc.conf  ${HOME}/.config/pp_mpv/script-opts/osc.conf.update
		echo "WARNING: osc.conf saved as osc.conf.update"
	fi
else
	cp script-opts/osc.conf  ${HOME}/.config/pp_mpv/script-opts
fi

if [ ! -f ${HOME}/.config/pp_mpv/scripts/autoload.lua ]; then
	# autoload is not installed
	echo -n "

	Would you like to install autoload (recommended)?

	autoload (credit: mpv) automatically loads playlist/directory entries before and after the selected file

	Y/n "

	read -n 1 -s ans

	echo $ans

	if [[ "$ans" = "Y" || "$ans" = "y" || "$ans" = "" ]]; then
		cp scripts/autoload.lua ${HOME}/.config/pp_mpv/scripts
		cp script-opts/autoload.conf  ${HOME}/.config/pp_mpv/script-opts
		echo -e "\nautoload installed\n"
	fi
else
	# autoload is already installed
	cp scripts/autoload.lua ${HOME}/.config/pp_mpv/scripts
	# test if autoload.conf already exists
	if [ -f  ${HOME}/.config/pp_mpv/script-opts/autoload.conf ]; then
		# file exists
		if ! cmp -s "scripts-opts/autoload.conf" "${HOME}/.config/pp_mpv/script-opts/autoload.conf"; then
			cp script-opts/autoload.conf  ${HOME}/.config/pp_mpv/script-opts/autoload.conf.update
			echo "WARNING: autoload.conf saved as autoload.conf.update"
		fi
	else
		cp script-opts/autoload.conf ${HOME}/.config/pp_mpv/script-opts
	fi
fi

if [ ! -f ${HOME}/.config/pp_mpv/scripts/coverart.lua ]; then
	# coverart is not installed
	echo -n "
	
	Would you like to install coverart?
	
	coverart (credit: https://github.com/CogentRedTester/mpv-coverart) automatically loads cover art files, or default image, for audio files.
	
	Y/n "
	
	read -n 1 -s ans

	echo $ans
	
	if [[ "$ans" = "Y" || "$ans" = "y" || "$ans" = "" ]]; then
		cp scripts/coverart.lua ${HOME}/.config/pp_mpv/scripts
		cp script-opts/coverart.conf ${HOME}/.config/pp_mpv/script-opts
		echo -e "\ncoverart installed\n"
	fi
else
	# coverart is already installed
	cp scripts/coverart.lua ${HOME}/.config/pp_mpv/scripts
	# test if coverart.conf already exists
	if [ -f  ${HOME}/.config/pp_mpv/script-opts/coverart.conf ]; then
		# file exists
		if ! cmp -s "script-opts/coverart.conf" "${HOME}/.config/pp_mpv/script-opts/coverart.conf"; then
			cp script-opts/coverart.conf  ${HOME}/.config/pp_mpv/script-opts/coverart.conf.update
			echo "WARNING: coverart.conf saved as coverart.conf.update"
		fi
	else
		cp script-opts/coverart.conf ${HOME}/.config/pp_mpv/script-opts
	fi
fi

if [ ! -f ${HOME}/.config/pp_mpv/scripts/thumbfast.lua ]; then
	# thumbfast is not installed
	echo -n "
	Would you like to install thumbfast?
	
	thumbfast (credit: https://github.com/po5/thumbfast) high-performance on-the-fly thumbnail viewer for video files.
	
	Y/n "
	
	read -n 1 -s ans

	echo $ans
	
	if [[ "$ans" = "Y" || "$ans" == "y" || "$ans" = "" ]]; then
		cp scripts/thumbfast.lua ${HOME}/.config/pp_mpv/scripts
		cp script-opts/thumbfast.conf ${HOME}/.config/pp_mpv/script-opts
		echo -e "\nthumbfast installed\n"
	fi
else 
	cp scripts/thumbfast.lua ${HOME}/.config/pp_mpv/scripts
	# test if thumbfast.conf already exists
	if [ -f  ${HOME}/.config/pp_mpv/script-opts/thumbfast.conf ]; then
		# file exists
		if ! cmp -s "script-opts/thumbfast.conf" "${HOME}/.config/pp_mpv/script-opts/thumbfast.conf"; then
			cp script-opts/thumbfast.conf  ${HOME}/.config/pp_mpv/script-opts/thumbfast.conf.update
			echo "WARNING: thumbfast.conf saved as thumbfast.conf.update"
		fi
	else
		cp script-opts/thumbfast.conf ${HOME}/.config/pp_mpv/script-opts
	fi
fi

echo -e "\nInstall done\n"
