#! /bin/bash

sudo rm /usr/bin/pinephone_mpv
sudo rm -rf /usr/share/pixmaps/pinephone_mpv
sudo rm  /usr/share/applications/pinephone_mpv.desktop
sudo rm -rf ~/.config/pp_mpv

echo -e  "\nTo reinstall pinephone_mpv download the source from https://gitlab.com/AndyM48/pinephone_mpv/ and run install.sh\n"
echo "Uninstall done"
